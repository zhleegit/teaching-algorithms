import HeapSort, LinearTimeSort , InsertionSort, MergeSort , QuickSort
from random import randint
def init_list(n):
    return [randint(0,500)  for _ in range(n)]

def testSort(a,b):
    if len(a)!=len(b):
        return "Not the same len"
    ans = []
    for i in range(len(a)):
        ans.append(a[i]==b[i])
    return all(ans) if "pass"  else "failure"



def run():
    a=init_list(50)
    print("get start")
    print('original: ',  a)
    a_default_sort=sorted(a)
    # a = InsertionSort.sort(a)
    a = HeapSort.sort(a)



    print('after sorting: ' , a)
    # print("default sort: ",  a_default_sort)
    print("Judge: ", testSort(a , a_default_sort))

if __name__=='__main__':
    run()
