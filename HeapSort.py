import numpy as np
import math

class MinHeap:

    def __init__(self, a):

        self.max_index = len(a) # of heap
        self.heap = np.zeros(self.max_index+1) # a[0] will not be used (0*2 cannot change to its child)
        for i,value in enumerate(a):
            self.heap[i+1] = value

        if self.max_index<=1:
            return

        self.buildMinHeap()

    def buildMinHeap(self):
        for key in range(math.floor(self.max_index/2) , -1 , -1):
            self.heapify(key)

    def heapify(self , key):
        left = key*2 
        right =  left +1

        minkey = left
        if right<=self.max_index and self.heap[right]<self.heap[minkey]:
            minkey = right
        if self.heap[minkey]<self.heap[key]:
            tmp = self.heap[minkey]
            self.heap[minkey] = self.heap[key]
            self.heap[key] = tmp
        else:
            minkey = key

        if key!=minkey and minkey<=math.floor(self.max_index/2):
            self.heapify(minkey)
        

    def pop(self):
        if self.max_index<=0:
            return None
        minvalue = self.heap[1]
        self.heap[0] =self.heap[self.max_index]
        self.max_index -=1
        self.heapify(0)
        return minvalue

    def heapSort(self):
        output = np.zeros(self.max_index)
        for i in range(self.max_index):
            output[i] = self.pop()
        return output.astype(int)

def sort(a):
    h = MinHeap(a)
    return h.heapSort()
