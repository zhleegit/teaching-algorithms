Algorithm for Skill Exchange
==
textbook: Introduction To Algorithms, 3rd edition
[Tenlong online book store](https://www.tenlong.com.tw/products/9780262533058)


Sorting
--


:::info
- Insertion sort
- Heap sort
- Quick sort
- Sorting in linear time
    - Counting sort
    - Radix sort
    - Bucket sort
- Merge sort
:::







Data Structure 
--
:::info
- Linked list
    - Stack 
    - Queue
- Hash table
- Binary search tree
- Red-Black tree
- B-tree
- Fibonacci heap
- Disjoint Set
:::


Greedy Algorithm
--
:::info
- Activity-selection problem
- Huffman code
:::

Dynamic Programming
--
:::info
- Matrix-chain multiplication
- Rod cutting
- Longest common subsqeuence
- Optimal binary search tree
:::


Graph Algorithm
--
:::info
- Representing a graph
- BFS
- DFS
- Topological sort
- Strongly connected components
- Single-source shortest path
- All-pairs shortest path
- maximum flow
:::


Selected Topics
--
:::info
- Multithread algorithms
- Matrix operations
- Linear programming
- Polynomials and FFT
- Number-theoretic algorithms
    - G.C.D
    - Chinese remainder theorem
    - RSA
- String mathing
- Approximation algorithm
:::