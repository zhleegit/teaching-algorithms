

def sort(a):
    if len(a)<=1:
        return a
    for i in range(1,len(a)):
        key = i
        j = i-1
        while j>=0 and a[j]>a[key]:
            tmp = a[j]
            a[j] = a[key]
            a[key] = tmp
            key = j
            j-=1
    return a


            


